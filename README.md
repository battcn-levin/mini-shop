# 小程序商城介绍

 mini-shop小程序商城，包括：分销（支持三级）、团购（拼多多模式）、秒杀、优惠券、等功能，前后端全部开源。做全网最开源、最稳定、功能做强大的开源小程序商城。

# mini-shop是一套完全开源的微信小程序商场系统，真正前后台全部开源，让那些半开源和收费的见鬼去吧！！！最新部署视频也免费上传到群里！！！

很多人想找一套真正完全开源的微信小程序商城而找不到，现在我们团队经过整合码云开源代码后重磅对出此套完全开源程序。希望大家可以支持我们，
我们会不断完善代码和推出新功能来让大家更好的使用。

# 团购、秒杀、分销、优惠券等活动已经完成

# 真正开源做码云最开源的小程序商城。

# 后续spring-cloud架构、团长模式等陆续上线




# 特点

* 免费完整开源：基于MIT协议，源代码完全开源，无商业限制,承诺将系统永久完整开源；
* 无BUG：经过严格测试，开箱即用；
* 编码优雅：代码结构清晰,注解非常详细，方便小伙伴们学习和使用。；
* 持久更新：会定期公布开发计划。并按计划提交新的功能；
* 活跃的QQ群，及时耐心的解答问题；
* 小程序商城 技术交流群号：897414796




# 面向对象

* mini-shop是企业在创立初期很好的技术基础框架，加快公司项目开发进度，当然也可以对现有的系统进行升级；
* 个人开发者也可以使用mini-shop承接外包项目；
* 初学JAVA的同学可以下载源代码来进行学习交流；
* 有需要定制的可以联系微信（2273803419）。



# 软件架构

* 核心框架：Spring boot 1.5.4.RELEASE
* 安全框架：Apache Shiro 1.2
* 视图框架：Spring MVC 4
* 持久层框架：MyBatis 3
* 数据库连接池：HikariCP
* 日志管理：SLF4J 1.7、Log4j
* JS框架：Vue 2.5.1，iview，layer 3.0.3，jquery 2.2.4，jqgrid 5.1.1
* CSS框架：Twitter bootstrap3.3.7。
* 富文本：froala_editor1.2.2



# 开发环境
建议开发者使用以下环境，这样避免版本带来的问题
* IDE:eclipse
* DB:Mysql5.8
* JDK:JAVA8
* WEB:Tomcat8





# 运行环境

* WEB服务器：Weblogic、Tomcat、WebSphere、JBoss、Jetty 等
* 数据库服务器：Mysql5.8
* 操作系统：Windows、Linux、Unix 等




# 快速体验

* 将mini-shop项目源码通过maven形式导入eclipse；
* 导入mini-shop.sql数据文件,注意：数据库使用utf-8编码；
* 修改application.yml文件中的数据库设置参数；
* 找到 ApplicationConfiguration类 运行main方法运行即可
* 管理员账号，用户名：默认 密码：默认




# 小程序部署：

* 打开小程序工具；
* 选择你下载的源代码wx-mall小程序项目；
* 输入你的AppID；
* 填写你的项目名称；
* 进入之后修改config文件夹里的api.js文件，把NewApiRootUrl改为你后台接口地址即刻运行。




# 小程序演示地址




![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005600_1d22b19d_748706.jpeg "170751_dac85a7a_81788.jpeg")


# 小程序演示效果
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005731_a499dff6_748706.png "QQ截图20190809214145.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005748_b8350b4c_748706.png "QQ截图20190809214205.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005758_863bfa6a_748706.png "QQ截图20190809214218.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005813_30cfee2f_748706.png "QQ截图20190809214241.png")





# 小程序后台管理系统演示效果
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005848_bfa7f0ff_748706.png "QQ截图20190809214606.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005902_9b442843_748706.png "QQ截图20190809214625.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005915_3f586428_748706.png "QQ截图20190809214709.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0810/005931_1de0f452_748706.png "145541_2a1e5aba_1293644.png")

# 如果你喜欢我们的开源请点个赞是对我们的奖励
# 如果你喜欢我们的开源请点个赞是对我们的奖励
# 如果你喜欢我们的开源请点个赞是对我们的奖励