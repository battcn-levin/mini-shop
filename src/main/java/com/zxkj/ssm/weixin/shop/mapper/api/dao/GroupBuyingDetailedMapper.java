package com.zxkj.ssm.weixin.shop.mapper.api.dao;
import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.GroupBuyingDetailedVo;

/**
 * Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2019-06-13 22:00:13
 */
public interface GroupBuyingDetailedMapper extends BaseDao<GroupBuyingDetailedVo> {

}
