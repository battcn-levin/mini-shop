package com.zxkj.ssm.weixin.shop.mapper.api.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.SearchHistoryVo;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiSearchHistoryMapper extends BaseDao<SearchHistoryVo> {
    int deleteByUserId(@Param("user_id") Long userId);
}
