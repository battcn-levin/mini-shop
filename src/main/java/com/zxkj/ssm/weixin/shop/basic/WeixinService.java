package com.zxkj.ssm.weixin.shop.basic;

import com.zxkj.ssm.weixin.shop.exception.WxErrorException;
import com.zxkj.ssm.weixin.shop.http.executor.RequestExecutor;

import java.util.Map;

/**
 *@Author 中芯信息科技
 *@Date 2018/7/11 9:19
 *@Description  封装了简单的 http 请求
 *@Version
 */
public interface WeixinService {
    /**
     * 简单的post请求
     * @param url
     * @param postData
     * @return
     * @throws WxErrorException
     */
    String post(String url, String postData) throws WxErrorException;


    /**
     * 简单的POST请求
     * @param url
     * @param map  请求数据为Map
     * @return
     * @throws WxErrorException
     */
    String post(String url, Map<?,?> map) throws WxErrorException;


    /**
     * 简单的Get请求
     * @param url
     * @param queryParam
     * @return
     * @throws WxErrorException
     */
    String get(String url, String queryParam) throws WxErrorException;


    /**
     * httpclient请求执行器
     * @param executor
     * @param uri
     * @param data
     * @return
     * @throws WxErrorException
     */
    <T, E> T execute(RequestExecutor<T, E> executor, String uri, E data) throws WxErrorException;

}
