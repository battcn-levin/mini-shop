package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.AdEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 09:37:35
 */
public interface AdDao extends BaseDao<AdEntity> {

}
