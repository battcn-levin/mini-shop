package com.zxkj.ssm.weixin.shop.qrcode;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 *二维码生成
 */

public class ZXingCodeUtil {
		/**
	     * 二维码图片添加logo
	     * 
	     * @param bim  图片流
	     * @param logoPic logo 图片物理位置
	     * @param logoConfig logo图片设置参数
	     * @throws Exception
	     */
	    private void addLogo_QRCode(BufferedImage bim, File logoPic, LogoConfig logoConfig) throws Exception {
	        try {
	            BufferedImage image = bim;
	            Graphics2D g = image.createGraphics();

	            BufferedImage logo = ImageIO.read(logoPic);//读取logo图片

	            // 设置logo大小 本人设置二维码图片的20%  因为过大会覆盖二维码
	            int widthLogo = logo.getWidth(null) > image.getWidth() * 2 / 10 ? (image.getWidth() * 2 / 10) : logo.getWidth(null), heightLogo = logo
	                    .getHeight(null) > image.getHeight() * 2 / 10 ? (image.getHeight() * 2 / 10) : logo.getWidth(null);

	            // 计算图片的放置位置
	            // logo放置中心
	            int x = (image.getWidth() - widthLogo) / 2;
	            int y = (image.getHeight() - heightLogo) / 2;
	            // 开始绘制图片
	            g.drawImage(logo, x, y, widthLogo, heightLogo, null);
	            g.drawRoundRect(x, y, widthLogo, heightLogo, 15, 15);
	            g.setStroke(new BasicStroke(logoConfig.getBorder()));
	            g.setColor(logoConfig.getBorderColor());
	            g.drawRect(x, y, widthLogo, heightLogo);

	            g.dispose();
	            logo.flush();
	            image.flush();

	        } catch (Exception e) {
	            throw e;
	        }
	    }
		/**
	     * 二维码的解析
	     * 
	     * @param image  图片文件流
	     * @return 解析后的result结果集
	     * @throws Exception
	     */

	    @SuppressWarnings("unchecked")
	    public Result parseQR_CODEImage(BufferedImage image) throws Exception {
	        // 设置解析
	        Result result = null;
	        try {
	            MultiFormatReader formatReader = new MultiFormatReader();

	            LuminanceSource source = new BufferedImageLuminanceSource(image);
	            Binarizer binarizer = new HybridBinarizer(source);
	            BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);

	            @SuppressWarnings("rawtypes")
	            Map hints = new HashMap();
	            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");

	            result = formatReader.decode(binaryBitmap, hints);

	            System.out.println("resultFormat = " + result.getBarcodeFormat());
	            System.out.println("resultText = " + result.getText());
	        } catch (Exception e) {
	            throw e;
	        }
	        return result;
	    }


		/**
	     * 生成二维码bufferedImage图片
	     * @param zxingconfig 二维码配置信息
	     * @return 生成后的 BufferedImage
	     * @throws Exception
	     */

	    public BufferedImage getQR_CODEBufferedImage(ZXingConfig zxingconfig) throws Exception {
	       //谷歌配置文件
	         MultiFormatWriter multiFormatWriter = null;
	        BitMatrix bm = null;
	        BufferedImage image = null;
	        try {
	            multiFormatWriter = new MultiFormatWriter();

	            // 参数顺序分别为 编码内容、 编码类型、生成的图片宽度 、生成图片高度、设置参数
	            bm = multiFormatWriter.encode(zxingconfig.getContent(), zxingconfig.getBarcodeformat(), zxingconfig.getWidth(), zxingconfig.getHeight(),
	                    zxingconfig.getHints());

	            int w = bm.getWidth();
	            int h = bm.getHeight();
	            image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

	            // 开始利用二维码创建Bitmapc图片 分别设为黑白两种颜色
	            for (int x = 0; x < w; x++) {
	                for (int y = 0; y < h; y++) {
	                    image.setRGB(x, y, bm.get(x, y) ? Color.BLACK.getRGB() : Color.WHITE.getRGB());
	                }
	            }

	            // 是否设置logo图片
	            if (zxingconfig.isLogoFlg()) {
	                this.addLogo_QRCode(image, new File(zxingconfig.getLogoPath()), zxingconfig.getLogoConfig());
	            }
	        } catch (WriterException e) {
	            throw e;
	        }
	        return image;
	    }


	    public Map<EncodeHintType, Object> getDecodeHintType() {
	        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
	        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
	        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
	        hints.put(EncodeHintType.MARGIN, 0);
	       // hints.put(EncodeHintType.MAX_SIZE, 350);
	        //hints.put(EncodeHintType.MIN_SIZE, 100);

	        return hints;
	    }

	}
