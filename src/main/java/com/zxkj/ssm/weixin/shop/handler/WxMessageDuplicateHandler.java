package com.zxkj.ssm.weixin.shop.handler;

/**
 *@Author 中芯信息科技
 *@Date 2018/7/11 21:45
 *@Description 消息重复检查器 微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次
 *@Version
 */
public interface WxMessageDuplicateHandler {
	  /**
	   * 普通消息：关于重试的消息排重，推荐使用msgid排重。
	   * 事件消息：关于重试的消息排重，推荐使用FromUserName + CreateTime 排重。
	   * @References http://mp.weixin.qq.com/wiki/2/5baf56ce4947d35003b86a9805634b1e.html
	   * <p>或者可以采取更简单的方式，如果有MsgId就用MsgId排重，如果没有就用FromUserName+CreateTime排重
	   * @param messageId messageId需要根据上面讲的方式构造
	   * @return 如果是重复消息，返回true，否则返回false
	   */
	 boolean isDuplicate(String messageId);
}
