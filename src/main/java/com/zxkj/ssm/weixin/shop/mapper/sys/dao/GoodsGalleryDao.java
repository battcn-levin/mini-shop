package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.GoodsGalleryEntity;

import java.util.Map;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-23 14:41:43
 */
public interface GoodsGalleryDao extends BaseDao<GoodsGalleryEntity> {
    int deleteByGoodsId(Map<String, Integer> map);
}
