package com.zxkj.ssm.weixin.shop.annotation;

import java.lang.annotation.*;

/**
 * 忽略Token验证
 *
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017-03-23 15:44
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreAuth {

}
