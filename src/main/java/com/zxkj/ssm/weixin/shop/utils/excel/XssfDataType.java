package com.zxkj.ssm.weixin.shop.utils.excel;

/**
 * XSSFDataType
 *
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017年10月28日 13:11:27
 */
public enum XssfDataType {
    BOOL, ERROR, FORMULA, INLINESTR, SSTINDEX, NUMBER, DATE, DATETIME, TIME,
}
