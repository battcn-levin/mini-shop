package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.HolidayEntity;

/**
 * Created by Administrator on 2019/8/6.
 */
public interface HolidayDao extends BaseDao<HolidayEntity> {
}
