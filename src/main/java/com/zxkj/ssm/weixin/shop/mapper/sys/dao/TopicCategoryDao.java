package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.TopicCategoryEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-20 15:41:56
 */
public interface TopicCategoryDao extends BaseDao<TopicCategoryEntity> {

}
