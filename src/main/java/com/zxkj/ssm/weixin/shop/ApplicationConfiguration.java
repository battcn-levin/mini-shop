package com.zxkj.ssm.weixin.shop;
import com.zaxxer.hikari.HikariDataSource;
import com.zxkj.ssm.weixin.shop.web.filter.XssFilter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

/**
 *
 * @Author 中芯信息科技
 * @Date 2018/7/11 9:04
 * @Version
 * @Description
 */
@SpringBootApplication
@PropertySources(value={@PropertySource("classpath:config/config.properties"),})
@MapperScan(value={"com.zxkj.ssm.weixin.shop.mapper.*.dao"})
@EnableScheduling
@EnableTransactionManagement(proxyTargetClass=true)
@ServletComponentScan(basePackageClasses = {XssFilter.class})
public class ApplicationConfiguration extends SpringBootServletInitializer {

	/**
	 * 外置tomcat入口
	 *
	 * @param builder
	 * @return
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ApplicationConfiguration.class);
	}

	public static void main(String[] args) {
		new SpringApplication(ApplicationConfiguration.class).run(args);
	}

	@Bean
	public PlatformTransactionManager txManager(HikariDataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}
}


