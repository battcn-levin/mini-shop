package com.zxkj.ssm.weixin.shop.config.api;
import com.zxkj.ssm.weixin.shop.service.api.ApiUserService;
import com.zxkj.ssm.weixin.shop.web.resolver.AppLoginUserHandlerMethodArgumentResolver;
import com.zxkj.ssm.weixin.shop.web.resolver.LoginUserHandlerMethodArgumentResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import java.util.List;
@Configuration
public class WebApiConfiguration extends WebMvcConfigurerAdapter {


    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(loginUserHandlerMethodArgumentResolver());
        argumentResolvers.add(appLoginUserHandlerMethodArgumentResolver());
    }

    @Bean
    public LoginUserHandlerMethodArgumentResolver loginUserHandlerMethodArgumentResolver(){
        LoginUserHandlerMethodArgumentResolver resolver=new LoginUserHandlerMethodArgumentResolver();
        resolver.setUserService(apiUserService());
        return resolver;
    }

    @Bean
    public AppLoginUserHandlerMethodArgumentResolver appLoginUserHandlerMethodArgumentResolver(){
        AppLoginUserHandlerMethodArgumentResolver loginUserArgumentResolver=new AppLoginUserHandlerMethodArgumentResolver();
        return loginUserArgumentResolver;
    }


    @Bean
    public ApiUserService apiUserService(){
        ApiUserService apiUserService=new ApiUserService();
        return apiUserService;
    }


    @Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory factory){
        return new RestTemplate(factory);
    }

    @Bean
    public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(5000);//单位为ms
        factory.setConnectTimeout(5000);//单位为ms
        return factory;
    }


}
