package com.zxkj.ssm.weixin.shop.service;

import com.zxkj.ssm.weixin.shop.entity.SysRoleEntity;
import com.zxkj.ssm.weixin.shop.entity.UserWindowDto;
import com.zxkj.ssm.weixin.shop.model.Page;

import java.util.List;
import java.util.Map;


/**
 * 角色
 *
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2016年9月18日 上午9:42:52
 */
public interface SysRoleService {

    SysRoleEntity queryObject(Long roleId);

    List<SysRoleEntity> queryList(Map<String, Object> map);

    int queryTotal(Map<String, Object> map);

    void save(SysRoleEntity role);

    void update(SysRoleEntity role);

    void deleteBatch(Long[] roleIds);

    /**
     * 查询用户创建的角色ID列表
     */
    List<Long> queryRoleIdList(Long createUserId);

    /**
     * 分页查询角色审批选择范围
     * @return
     */
    Page<UserWindowDto> queryPageByDto(UserWindowDto userWindowDto, int pageNmu);
}
