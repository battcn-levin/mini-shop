package com.zxkj.ssm.weixin.shop.mapper.api.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.GoodsGalleryVo;

/**
 * 
 * 
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiGoodsGalleryMapper extends BaseDao<GoodsGalleryVo> {
	
}
