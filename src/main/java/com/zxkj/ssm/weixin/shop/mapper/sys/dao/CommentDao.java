package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.CommentEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-28 17:03:40
 */
public interface CommentDao extends BaseDao<CommentEntity> {

}
