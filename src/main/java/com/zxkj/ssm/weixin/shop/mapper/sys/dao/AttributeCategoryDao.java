package com.zxkj.ssm.weixin.shop.mapper.sys.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.AttributeCategoryEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-17 16:13:27
 */
public interface AttributeCategoryDao extends BaseDao<AttributeCategoryEntity> {

}
