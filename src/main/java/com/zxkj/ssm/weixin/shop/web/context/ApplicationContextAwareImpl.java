package com.zxkj.ssm.weixin.shop.web.context;
import com.zxkj.ssm.weixin.shop.utils.SpringContextUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *@Author 程序媛
 *@Date 2018/7/11 9:04
 *@Description 获取容器实例
 *@Version
 */
@Component
public class ApplicationContextAwareImpl  implements ApplicationContextAware {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        SpringContextUtils.applicationContext = applicationContext;
    }

}
