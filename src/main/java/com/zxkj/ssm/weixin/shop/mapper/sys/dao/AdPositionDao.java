package com.zxkj.ssm.weixin.shop.mapper.sys.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.AdPositionEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 12:02:42
 */
public interface AdPositionDao extends BaseDao<AdPositionEntity> {

}
