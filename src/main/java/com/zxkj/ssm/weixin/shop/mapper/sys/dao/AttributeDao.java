package com.zxkj.ssm.weixin.shop.mapper.sys.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.AttributeEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-17 16:48:17
 */
public interface AttributeDao extends BaseDao<AttributeEntity> {

}
