package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.CouponGoodsEntity;

/**
 * 优惠券关联商品Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-29 21:50:17
 */
public interface CouponGoodsDao extends BaseDao<CouponGoodsEntity> {

}
