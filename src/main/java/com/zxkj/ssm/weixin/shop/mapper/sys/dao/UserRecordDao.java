package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.UserRecordEntity;

/**
 * 会员Dao
 *
 * @author 周海生
 * @email 939961241@qq.com
 * @date 2017-08-16 15:02:28
 */
public interface UserRecordDao extends BaseDao<UserRecordEntity> {

}
