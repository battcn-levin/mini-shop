package com.zxkj.ssm.weixin.shop.mapper.api.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.UserCouponVo;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017-08-11 09:16:47
 */
public interface ApiUserCouponMapper extends BaseDao<UserCouponVo> {
    UserCouponVo queryByCouponNumber(@Param("coupon_number") String coupon_number);

    int queryUserGetTotal(Map userParams);
    
    void updateCouponStatus(UserCouponVo vo);
}
