package com.zxkj.ssm.weixin.shop.mapper.sys.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.BrandEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 17:59:15
 */
public interface BrandDao extends BaseDao<BrandEntity> {

}
