package com.zxkj.ssm.weixin.shop.utils;

import org.apache.http.client.HttpClient;

public class WeixinFinalValue {
	public static final String  WX_PAY_CER="/WEB-INF/cer/apiclient_cert.p12";//证书位置
	public static final String WXPAYSDK_VERSION = "WXPaySDK/3.0.9";
	public static final String USER_AGENT = WXPAYSDK_VERSION +
			" (" + System.getProperty("os.arch") + " " + System.getProperty("os.name") + " " + System.getProperty("os.version") +
			") Java/" + System.getProperty("java.version") + " HttpClient/" + HttpClient.class.getPackage().getImplementationVersion();



	public final static String APPID="wx292e1dfb5c3648d4";
		public final static String APPSECRET="a1295a46bbb170746aa1363a25bd716d";
		
		/**
		 * 微信消息类型  
		 */
		
		public final static String MSG_TEXT_TYPE = "text";//文本消息
		public final static String MSG_IMAGE_TYPE = "image";//图片消息
		public final static String MSG_VOICE_TYPE = "voice";//语音消息
		public final static String MSG_VIDEO_TYPE = "video";//视频
		public final static String MSG_SHORTVIDEO_TYPE = "shortvideo";//小视频为shortvideo
		public final static String MSG_LOCATION_TYPE = "location";//地理位置
		public final static String MSG_EVENT_TYPE = "event";//事件 
		public final static String GRAPHIC_TYPE="news";
	
		/**
		 * 群发消息的消息类型
		 */
		
		public static final String MASS_MSG_NEWS = "mpnews";
		public static final String MASS_MSG_TEXT = "text";
		public static final String MASS_MSG_VOICE = "voice";
		public static final String MASS_MSG_IMAGE = "image";
		public static final String MASS_MSG_VIDEO = "mpvideo";
		
	    /**
	     * 微信端推送过来的Event事件类型
	     */
		
		public static final String EVT_SUBSCRIBE = "subscribe";
		public static final String EVT_UNSUBSCRIBE = "unsubscribe";
		public static final String EVT_SCAN = "SCAN";
		public static final String EVT_LOCATION = "LOCATION";
		public static final String EVT_CLICK = "CLICK";
		public static final String EVT_VIEW = "VIEW";
		public static final String EVT_MASS_SEND_JOB_FINISH = "MASSSENDJOBFINISH";
		public static final String EVT_SCANCODE_PUSH = "scancode_push";
	  	public static final String EVT_SCANCODE_WAITMSG = "scancode_waitmsg";
	  	public static final String EVT_PIC_SYSPHOTO = "pic_sysphoto";
	  	public static final String EVT_PIC_PHOTO_OR_ALBUM = "pic_photo_or_album";
	  	public static final String EVT_PIC_WEIXIN = "pic_weixin";
	  	public static final String EVT_LOCATION_SELECT = "location_select";
	  	public static final String EVT_TEMPLATESENDJOBFINISH = "TEMPLATESENDJOBFINISH";
	  	public static final String EVT_ENTER_AGENT = "enter_agent";
	  	public static final String EVT_CARD_PASS_CHECK = "card_pass_check";
	  	public static final String EVT_CARD_NOT_PASS_CHECK = "card_not_pass_check";
	  	public static final String EVT_USER_GET_CARD = "user_get_card";
	  	public static final String EVT_USER_DEL_CARD = "user_del_card";
	  	public static final String EVT_USER_CONSUME_CARD = "user_consume_card";
	  	public static final String EVT_USER_SHAKE="ShakearoundUserShake";
	  	public static final String EVT_USER_PAY_FROM_PAY_CELL = "user_pay_from_pay_cell";
	  	public static final String EVT_USER_VIEW_CARD = "user_view_card";
	  	public static final String EVT_USER_ENTER_SESSION_FROM_CARD = "user_enter_session_from_card";
	  	public static final String EVT_CARD_SKU_REMIND = "card_sku_remind"; // 库存报警
	  	public static final String EVT_KF_CREATE_SESSION = "kf_create_session"; // 客服接入会话
	  	public static final String EVT_KF_CLOSE_SESSION = "kf_close_session"; // 客服关闭会话
	  	public static final String EVT_KF_SWITCH_SESSION = "kf_switch_session"; // 客服转接会话
	  	public static final String EVT_POI_CHECK_NOTIFY = "poi_check_notify"; //门店审核事件推送
	  	public static final String EVT_QUALIFICATION_VERIFY_FAIL="qualification_verify_fail";//// 资质认证失败 
	  	public static final String EVT_NAMING_VERIFY_FAIL="naming_verify_fail";//名称认证失败
	  	public static final String EVT_QUALIFICATION_VERIFY_SUCCESS="qualification_verify_success";// 资质认证成功 
		public static final String EVT_NAMING_VERIFY_SUCCESS="naming_verify_success";//名称认证成功 
		public static final String EVT_ANNUAL_RENEW="annual_renew";//年审通知 
		public static final String EVT_VERIFY_EXPIRED="verify_expired";// 认证过期失效通知
		
	  	/********oauth2网页授权的scope************/
	  	 
	  	/**
	     * 不弹出授权页面，直接跳转，只能获取用户openid
	     */
	    public static final String OAUTH2_SCOPE_BASE = "snsapi_base";
	    /**
	     * 弹出授权页面，可通过openid拿到昵称、性别、所在地。并且，即使在未关注的情况下，只要用户授权，也能获取其信息
	     */
	    public static final String OAUTH2_SCOPE_USER_INFO = "snsapi_userinfo";
	    
	    /*** 自定义菜单的按钮类型***/
	   
	    /**
	     * 跳转URL
	     */
	    public static final String BUTTON_VIEW = "view";
	    /**
	     * 点击推事件
	     */
	    public static final String BUTTON_CLICK = "click";
	    /**
	     * 扫码推事件
	     */
	    public static final String BUTTON_SCANCODE_PUSH = "scancode_push";
	    /**
	     * 扫码推事件且弹出“消息接收中”提示框
	     */
	    public static final String BUTTON_SCANCODE_WAITMSG = "scancode_waitmsg";
	    /**
	     * 弹出系统拍照发图
	     */
	    public static final String BUTTON_PIC_SYSPHOTO = "pic_sysphoto";
	    /**
	     * 弹出拍照或者相册发图
	     */
	    public static final String BUTTON_PIC_PHOTO_OR_ALBUM = "pic_photo_or_album";
	    /**
	     * 弹出微信相册发图器
	     */
	    public static final String BUTTON_PIC_WEIXIN = "pic_weixin";
	    /**
	     * 弹出地理位置选择器
	     */
	    public static final String BUTTON_LOCATION_SELECT = "location_select";
	    /**
	     * 下发消息（除文本消息）
	     */
	    public static final String BUTTON_MEDIA_ID = "media_id";
	    /**
	     * 跳转图文消息URL
	     */
	    public static final String BUTTON_VIEW_LIMITED = "view_limited";
	    
	/**=======================获取 AccessToken=======================================**/	
	
	/** access_token是公众号的全局唯一票据，公众号调用各接口时都需使用access_token。开发者需要进行妥善保存。access_token的存储至少要保留512个字符空间。 access_token的有效期目前为2个小时，需定时刷新， 重复获取将导致上次获取的access_token失效。*/
	public final static String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	
	/**=======================素材管理===============================================**/	
	
	/**
	 *	上传的临时多媒体文件有格式和大小限制，如下：
	 *		图片（image）: 2M，支持bmp/png/jpeg/jpg/gif格式
	 *		语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式
	 *		视频（video）：10MB，支持MP4格式
	 *		缩略图（thumb）：64KB，支持JPG格式
	 */
	
	/**
	 * 新增临时素材
	 */ 
	public final static String ADD_MATERIAL_LS="https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
	/**
	 * 新增其他类型永久素材
	 */
	public final static String ADD_MATERIAL_YJ="https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE";
	/**
	 * 上传图文消息内的图片获取URL 本接口所上传的图片不占用公众号的素材库中图片数量的5000个的限制。图片仅支持jpg/png格式，大小必须在1MB以下。(永久素材范围内)
	 */
	public final static String ADD_MATERIAL_URL="https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN";
	/**
	 *新增图文素材
	 */
	public final static String GRAPHIC_LS="https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=ACCESS_TOKEN";
	/**
	 * 新增永久图文素材 注意：新增永久素材时   用到的多媒体素材 必须保证是永久素材 
	 */
	public final static String GRAPHIC_YJ="https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN";
	/**
	 *修改永久图文素材
	 */
	public final static String GRAPHIC_UPDATE_YJ="https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=ACCESS_TOKEN";
	
	/**
	 * 删除永久素材
	 */
	public final static String  DELETE_MATERIAL="https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=ACCESS_TOKEN";
	
	/**
	 * 根据media_id  获取临时素材
	 */
	public final static String GET_MEDIA="https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
	/**
	 *获取永久素材
	 */	
	public final static String GET_MATERIAL_YJ="https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=ACCESS_TOKEN";
	/**
	 *获取永久素列表   在新增了永久素材后，开发者可以分类型获取永久素材的列表。
	 */	
	public final static String GET_MATERIAL_LIST="https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=ACCESS_TOKEN";
	
	/**
	 * 预览接口*开发者可通过该接口发送消息给指定用户，在手机端查看消息的样式和排版。为了满足第三方平台开发者的需求，在保留对openID预览能力的同时，增加了对指定微信号发送预览的能力，但该能力每日调用次数有限制（100次），请勿滥用。
	 */
	public final static String GRAPHIC_MSG_PREVIEW="https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=ACCESS_TOKEN";
	
	/**======================模板消息==============================================**/	
	
	/**
	 * 模板消息
	 */
	public final static String SEND_TEMPLATE_MSG = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
	
	/**=======================分组管理==============================================**/	
	
	/**
	 * 添加分组
	 */
	public final static String ADD_GROUP = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=ACCESS_TOKEN";
	/**
	 * 查询所有分组
	 */
	public final static String QUERY_ALL_GROUP = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=ACCESS_TOKEN";
	/**
	 *查询用户所在分组
	 */
	public final static String QUERY_USER_GROUP = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=ACCESS_TOKEN";
	/**
	 *更新分组 
	 */
	public final static String UPDATE_GROUP_NAME = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=ACCESS_TOKEN";
	/**
	 *移动用户分组
	 */
	public final static String MOVE_USER_GROUP = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=ACCESS_TOKEN";
	/**
	 *移动分组
	 */
	public final static String MOVE_USERS_GROUP = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=ACCESS_TOKEN";
	/**
	 *删除分组
	 */
	public final static String DELETE_GROUP = "https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=ACCESS_TOKEN";
	/**
	 *查询所有用户
	 */
	public final static String GET_USER_LIST="https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN";
	/**
	 *查询单个用户
	 */
	public final static String GET_USER_DETAILS="https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
	
	/**=====================微信菜单===============================================**/
	
	/**
	 *发布菜单到微信
	 */
	public final static String MENU_ADD = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	/**
	 *菜单查询
	 */
	public final static String MENU_QUERY = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN";
	/**
	 *菜单删除
	 */
	public final static String MENU_DELETE="https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";
	
	/**=======================网页授权============================================**/	
	
	/** 
	 *网页授权url如果用户在微信客户端中访问第三方网页，公众号可以通过微信网页授权机制，来获取用户基本信息，进而实现业务逻辑
	 */
	public final static String AUTHC_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
	
	/**
	 * 获取code后，请求以下链接获取access_token：
	 */
	public final static String AUTHC_GET_ACCESS_TOKEN="https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
	/**
	 * 拉取用户信息
	 */
	public final static String AUTHC_GET_USER_INFO="https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
	
	/**
	  *根据code 获取openId
	  */
	public final static String AUTH_GET_OPENID = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
	
	/**=======================JS-SDK============================================**/	
	
	/**
	 *跟根据AccessToken获取 js接口分享的唯一票据  jsapi_ticket
	 */
	public final static  String TICKET_GET="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
	
	/**=======================群发功能===========================================**/	
	
	/***
	 *根据OpenId列表进行群发
	 */
	public final static String MASS_SEND_OPENID_LIST="https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=ACCESS_TOKEN";
	
}
