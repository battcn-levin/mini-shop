package com.zxkj.ssm.weixin.shop.config.quartz;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Properties;
    @Configuration
    public class QuartzSchedulerCofiguration extends WebMvcConfigurerAdapter {
    @Autowired
    private  HikariDataSource dataSource;
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(){
        SchedulerFactoryBean schedulerFactoryBean=new SchedulerFactoryBean();
        schedulerFactoryBean.setDataSource(dataSource);
        Properties quartzProperties =new Properties();
        quartzProperties.setProperty("org.quartz.scheduler.instanceName","PlatformScheduler");
        quartzProperties.setProperty("org.quartz.scheduler.instanceId","AUTO");
        quartzProperties.setProperty("org.quartz.threadPool.class","org.quartz.simpl.SimpleThreadPool");
        quartzProperties.setProperty("org.quartz.threadPool.threadCount","20");
        quartzProperties.setProperty("org.quartz.threadPool.threadPriority","5");
        quartzProperties.setProperty("org.quartz.jobStore.class","org.quartz.impl.jdbcjobstore.JobStoreTX");
        quartzProperties.setProperty("org.quartz.jobStore.isClustered","true");
        quartzProperties.setProperty("org.quartz.jobStore.clusterCheckinInterval","15000");
        quartzProperties.setProperty("org.quartz.jobStore.maxMisfiresToHandleAtATime","1");
        quartzProperties.setProperty("org.quartz.jobStore.misfireThreshold","12000");
        quartzProperties.setProperty("org.quartz.jobStore.tablePrefix","QRTZ_");
        schedulerFactoryBean.setQuartzProperties(quartzProperties);
        schedulerFactoryBean.setSchedulerName("PlatformScheduler");
        schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContextKey");
        schedulerFactoryBean.setOverwriteExistingJobs(true);
        schedulerFactoryBean.setStartupDelay(30);
        schedulerFactoryBean.setAutoStartup(true);
        return schedulerFactoryBean;
    }


}
