package com.zxkj.ssm.weixin.shop.utils;

import java.util.Map;

public interface DealMapValueHelper {
	void dealValue(String key, Map<String, Object> map);
}
