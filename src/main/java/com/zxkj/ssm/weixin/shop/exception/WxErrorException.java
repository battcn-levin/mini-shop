package com.zxkj.ssm.weixin.shop.exception;
public class WxErrorException extends Exception {

  private static final long serialVersionUID = -6357149550353160810L;
  private String message;
  private WxError error;

  public WxErrorException(WxError error) {
    super(error.toString());
    this.error = error;
  }

  public WxErrorException(String  message) {
    super(message);
    this.message=message;
  }

  public WxErrorException(WxError error, Throwable cause) {
    super(error.toString(), cause);
    this.error = error;
  }

  public WxError getError() {
    return this.error;
  }


}
