package com.zxkj.ssm.weixin.shop.mapper.api.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.OrderGoodsVo;

import java.util.List;

/**
 * 
 * 
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiOrderGoodsMapper extends BaseDao<OrderGoodsVo> {
	List<OrderGoodsVo> queryInvalidOrder();
}
