package com.zxkj.ssm.weixin.shop.web.task;

import com.zxkj.ssm.weixin.shop.service.SysOssService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 测试定时任务(演示Demo，可删除)
 * <p>
 * testTask为spring bean的名称
 *
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2016年11月30日 下午1:34:24
 */
@Component("testTask")
public class TestTask {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SysOssService sysOssService;

    public void test(String params) {
        logger.info("我是带参数的test方法，正在被执行，参数为：" + params);
    }

    public void test2() {
        logger.info("我是不带参数的test2方法，正在被执行");
    }
}
