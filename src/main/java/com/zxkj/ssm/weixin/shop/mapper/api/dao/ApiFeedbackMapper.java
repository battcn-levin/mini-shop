package com.zxkj.ssm.weixin.shop.mapper.api.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.FeedbackVo;

/**
 * 
 * 
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017-08-11 09:14:26
 */
public interface ApiFeedbackMapper extends BaseDao<FeedbackVo> {
	
}
