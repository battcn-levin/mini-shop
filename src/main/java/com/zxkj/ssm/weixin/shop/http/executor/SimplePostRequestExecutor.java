package com.zxkj.ssm.weixin.shop.http.executor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.zxkj.ssm.weixin.shop.exception.WxErrorException;
import com.zxkj.ssm.weixin.shop.handler.Utf8ResponseHandler;
import lombok.extern.log4j.Log4j;
import org.apache.http.Consts;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import  static com.zxkj.ssm.weixin.shop.basic.Basic.notEmpty;

/**
 *@Author 中芯信息科技
 *@Date 2018/7/11 9:11
 *@Description 简单的POST请求执行器，请求的参数是String, 返回的结果也是String
 *@Version
 */
@Log4j
public class SimplePostRequestExecutor implements RequestExecutor<String, String> {

  @Override
  public String execute(CloseableHttpClient httpclient, HttpHost httpProxy, String url, String postEntity) throws WxErrorException, IOException {
	    HttpPost httpPost = new HttpPost(url);
	    if (httpProxy != null) {
	      RequestConfig config = RequestConfig.custom().setProxy(httpProxy).build();
	      httpPost.setConfig(config);
	    }
	   if(notEmpty(postEntity)){
	      StringEntity entity = new StringEntity(postEntity, Consts.UTF_8);
		   log.info("请求URL:{}"+url+"请求数据：{}"+postEntity);

		   httpPost.setEntity(entity);
	      try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
	    	   return Utf8ResponseHandler.INSTANCE.handleResponse(response);
	      } finally {
	    	  httpPost.releaseConnection();
	      }
	    }
   	throw new WxErrorException("请求数据不能为空!");
  }

}
