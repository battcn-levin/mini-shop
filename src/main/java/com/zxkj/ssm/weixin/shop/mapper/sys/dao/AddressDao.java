package com.zxkj.ssm.weixin.shop.mapper.sys.dao;


import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.AddressEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-16 17:22:46
 */
public interface AddressDao extends BaseDao<AddressEntity> {

}
