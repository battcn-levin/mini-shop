package com.zxkj.ssm.weixin.shop.qrcode;
import java.util.Map;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;

/**
 * 二维码配置信息
 */


public class ZXingConfig {
    private boolean logoFlg = false;    //是否添加logo图片
    private String content; // 二维码编码内容
    private BarcodeFormat barcodeformat = BarcodeFormat.QR_CODE;
    private int width = 200;    // 生成图片宽度
    private int height = 200;   //  生成图片高度
    private Map<EncodeHintType, ?> hints;   //设置参数
    private String logoPath;    // logo图片路径
    private String putPath; //图片输出路径
    private LogoConfig LogoConfig;  // logo图片参数

    public boolean isLogoFlg() {
        return logoFlg;
    }


    public void setLogoFlg(boolean logoFlg) {
        this.logoFlg = logoFlg;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public BarcodeFormat getBarcodeformat() {
        return barcodeformat;
    }


    public void setBarcodeformat(BarcodeFormat barcodeformat) {
        this.barcodeformat = barcodeformat;
    }


    public int getWidth() {
        return width;
    }


    public void setWidth(int width) {
        this.width = width;
    }


    public int getHeight() {
        return height;
    }


    public void setHeight(int height) {
        this.height = height;
    }

    public Map<EncodeHintType, ?> getHints() {
        return hints;
    }

    public void setHints(Map<EncodeHintType, ?> hints) {
        this.hints = hints;
    }


    public String getLogoPath() {
        return logoPath;
    }


    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }


    public String getPutPath() {
        return putPath;
    }


    public void setPutPath(String putPath) {
        this.putPath = putPath;
    }


    public LogoConfig getLogoConfig() {
        return LogoConfig;
    }


    public void setLogoConfig(LogoConfig logoConfig) {
        LogoConfig = logoConfig;
    }

}
