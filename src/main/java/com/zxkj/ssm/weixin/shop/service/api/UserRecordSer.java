package com.zxkj.ssm.weixin.shop.service.api;
import com.zxkj.ssm.weixin.shop.mapper.api.dao.UserRecordMapper;
import com.zxkj.ssm.weixin.shop.entity.vo.UserRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <br>
 */
@Service
public class UserRecordSer  {
	
	 private Logger logger = LoggerFactory.getLogger(getClass());	
	
	@Autowired
	private UserRecordMapper userRecordDao;
	
	public UserRecordMapper getEntityMapper(){    	
    	return userRecordDao;
    }

    public void save(UserRecord ur) {
    	userRecordDao.insert(ur);
    }
	
}
