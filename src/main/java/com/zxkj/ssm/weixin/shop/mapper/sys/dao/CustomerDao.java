package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.CustomerEntity;

import java.util.List;
import java.util.Map;

/**
 * 客户Dao
 */
public interface CustomerDao extends BaseDao<CustomerEntity> {
	  List<CustomerEntity> query2List(Map<String, Object> map);
	 int query2Total(Map<String, Object> map);
}
