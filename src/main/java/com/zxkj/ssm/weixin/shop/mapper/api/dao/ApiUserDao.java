package com.zxkj.ssm.weixin.shop.mapper.api.dao;
import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.UserVo;
public interface ApiUserDao extends BaseDao<UserVo> {
}
