package com.zxkj.ssm.weixin.shop.config.mvc;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import com.zxkj.ssm.weixin.shop.web.interceptor.AuthorizationInterceptor;
import com.zxkj.ssm.weixin.shop.web.interceptor.LogInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *@Author 中芯信息科技
 *@Date 2018/7/20 13:25
 *@Description 配置StringHttpMessageConverter以支持  ResponseEntity<?>
 *            使用ResponseEntity<?>  return ResponseEntity.ok(datas) 提示错误Could not find acceptable representation
 *@Version
 */
@Configuration
public class WebMvcConfiguration  extends WebMvcConfigurerAdapter{
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/js/**").addResourceLocations("/js/");
		registry.addResourceHandler("/statics/**").addResourceLocations("/statics/");
		super.addResourceHandlers(registry);
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LogInterceptor()).addPathPatterns("/**")//
		 .excludePathPatterns("/statics/**","/goodQrCode/**","/**/**.html","/**/**.js");
		registry.addInterceptor(new AuthorizationInterceptor()).addPathPatterns("/api/**");
		super.addInterceptors(registry);

	}

	@Bean
	public HttpMessageConverter<String> responseBodyConverter() {
		StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
		return converter;
	}
	@Bean
	public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter(){
	return 	new MappingJackson2HttpMessageConverter();
	}


	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		super.configureMessageConverters(converters);
		converters.add(responseBodyConverter());
		converters.add(mappingJacksonHttpMessageConverter());
	}

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(false);
	}

	@Bean
	public VelocityViewResolver velocityViewResolver(){
		VelocityViewResolver resolver=new VelocityViewResolver();
		resolver.setContentType("text/html;charset=UTF-8");
		resolver.setViewNames("*.html");
		resolver.setDateToolAttribute("date");
		resolver.setNumberToolAttribute("number");
		resolver.setToolboxConfigLocation("/WEB-INF/velocity-toolbox.xml");
		resolver.setExposeRequestAttributes(true);
		resolver.setRequestContextAttribute("rc");
		resolver.setOrder(0);
		return resolver;
	}

	@Bean
	public VelocityConfigurer velocityConfigurer(){
		VelocityConfigurer configurer=new VelocityConfigurer();
		configurer.setResourceLoaderPath("/WEB-INF/page/");
		Map<String,Object> propertiesMap=new HashMap<String,Object>();
		propertiesMap.put("input.encoding","UTF-8");
		propertiesMap.put("output.encoding","UTF-8");
		propertiesMap.put("contentType","text/html;charset=UTF-8");
		configurer.setVelocityPropertiesMap(propertiesMap);
		return configurer;
	}


	@Bean
	public CommonsMultipartResolver commonsMultipartResolver(){
		CommonsMultipartResolver commonsMultipartResolver=new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(1000000000);
		return  commonsMultipartResolver;
	}
	@Bean
	@Scope("singleton")
	public DefaultKaptcha defaultKaptcha(){
		DefaultKaptcha kaptcha=new DefaultKaptcha();
		Properties properties =new Properties();
		properties.setProperty("kaptcha.border","no");
		properties.setProperty("kaptcha.textproducer.font.color","black");
		properties.setProperty("kaptcha.textproducer.char.space","4");
		properties.setProperty("kaptcha.textproducer.char.length","4");
		properties.setProperty("kaptcha.textproducer.char.string","123456789");
		kaptcha.setConfig(new Config(properties));
		return kaptcha;
	}


	@Bean
	public JavaMailSenderImpl javaMailSenderImpl(){
		JavaMailSenderImpl mailSender=new JavaMailSenderImpl();
		mailSender.setHost("smtp.139.com");
		mailSender.setPort(25);
		mailSender.setUsername("15821698254");
		mailSender.setPassword("yaohui");
		Properties properties =new Properties();
		properties.setProperty("mail.smtp.auth","true");
		mailSender.setJavaMailProperties(properties);
		return mailSender;
	}
	@Bean
	public org.springframework.mail.SimpleMailMessage simpleMailMessage(){
		SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
		simpleMailMessage.setFrom("15821698254@139.com");
		simpleMailMessage.setSubject("今天日代运维列表");
		return simpleMailMessage;
	}

}
