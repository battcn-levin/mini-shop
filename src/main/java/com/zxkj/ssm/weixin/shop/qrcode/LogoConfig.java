
package com.zxkj.ssm.weixin.shop.qrcode;

import java.awt.Color;
	public class LogoConfig {
	    public static final Color DEFAULT_BORDERCOLOR = Color.WHITE;   //logo默认边框颜色
	    public static final int DEFAULT_BORDER = 2; // logo 默认边框宽度
	    public static final int DEFAULT_LOGOPART = 5;   // logo默认大小为照片的1/5

	    private final int border = DEFAULT_BORDER;  // 默认边框宽度
	    private final Color borderColor; //边框颜色
	    private final int logoPart; //边框外围宽度


		/**
		 * 二维码无参构造函数 默认设置logo图片底色白色宽度
		 */

	    public LogoConfig() {
	        this(DEFAULT_BORDERCOLOR, DEFAULT_LOGOPART);
	    }


		/**
	     * 有参构造函数
	     * 
	     * @param borderColor 边框颜色
	     * @param logoPart 边宽宽度
	     */

	    public LogoConfig(Color borderColor, int logoPart) {
	        this.borderColor = borderColor;
	        this.logoPart = logoPart;
	    }


	    public Color getBorderColor() {
	        return borderColor;
	    }

	    public int getBorder() {
	        return border;
	    }

	    public int getLogoPart() {
	        return logoPart;
	    }
}

