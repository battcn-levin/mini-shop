package com.zxkj.ssm.weixin.shop.mapper.api.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.OrderVo;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author cxy
 * @email 2273803419@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiOrderMapper extends BaseDao<OrderVo> {
	public void updateStatus(OrderVo vo);
	public List<OrderVo> queryWaitList();
	public List<OrderVo> queryFxList();
	public List<OrderVo> queryByAllOrderId(String allOrderId);

    List<OrderVo> queryGroupBuyRefundList(Map map);
}
