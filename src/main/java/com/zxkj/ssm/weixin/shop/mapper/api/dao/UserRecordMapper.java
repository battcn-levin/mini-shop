package com.zxkj.ssm.weixin.shop.mapper.api.dao;
import com.zxkj.ssm.weixin.shop.basic.EntityDao;
import com.zxkj.ssm.weixin.shop.entity.vo.UserRecord;

/**
 * <br>
 */
public interface UserRecordMapper extends EntityDao<UserRecord,Integer> {
	
}
