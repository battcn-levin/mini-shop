package com.zxkj.ssm.weixin.shop.handler;
import lombok.extern.log4j.Log4j;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
/**
 *@Author 中芯信息科技
 *@Date 2018/7/11 9:09
 *@Description http  响应处理
 *@Version
 */
@Log4j
public class Utf8ResponseHandler implements ResponseHandler<String> {
  public static final ResponseHandler<String> INSTANCE = new Utf8ResponseHandler();
  @Override
  public String handleResponse(final HttpResponse response) throws IOException {
    final StatusLine statusLine = response.getStatusLine();
    final HttpEntity entity = response.getEntity();
    if (statusLine.getStatusCode() >= 300) {
      EntityUtils.consume(entity);
      throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
    }
    return entity == null ? null : EntityUtils.toString(entity, Consts.UTF_8);
  }
}
