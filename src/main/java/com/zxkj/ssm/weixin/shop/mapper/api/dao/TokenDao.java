package com.zxkj.ssm.weixin.shop.mapper.api.dao;
import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.vo.TokenEntity;

/**
 * Created by Administrator on 2019/8/6.
 */
public interface TokenDao extends BaseDao<TokenEntity> {
}
