package com.zxkj.ssm.weixin.shop.mapper.sys.dao;

import com.zxkj.ssm.weixin.shop.basic.BaseDao;
import com.zxkj.ssm.weixin.shop.entity.UpkeepEntity;

/**
 * 维护历史Dao
 */
public interface UpkeepDao extends BaseDao<UpkeepEntity> {

}
