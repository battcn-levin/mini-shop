package com.zxkj.ssm.weixin.shop.http.executor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import com.zxkj.ssm.weixin.shop.entity.vo.QrCodeTicketVo;
import com.zxkj.ssm.weixin.shop.exception.WxErrorException;
import com.zxkj.ssm.weixin.shop.handler.Utf8ResponseHandler;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;

/**
 * 获得QrCode图片 请求执行器
 * @author chanjarster
 *
 */
public class QrCodeRequestExecutor implements RequestExecutor<InputStream, QrCodeTicketVo> {
  @Override
  public InputStream execute(CloseableHttpClient httpclient, HttpHost httpProxy, String uri,QrCodeTicketVo ticket) throws WxErrorException, IOException {
    if (ticket != null) {
      if (uri.indexOf('?') == -1) {
        uri += '?';
      }
      uri += uri.endsWith("?") 
          ? "ticket=" + URLEncoder.encode(ticket.getTicket(), "UTF-8") 
          : "&ticket=" + URLEncoder.encode(ticket.getTicket(), "UTF-8");
    }
    
    HttpGet httpGet = new HttpGet(uri);
    if (httpProxy != null) {
      RequestConfig config = RequestConfig.custom().setProxy(httpProxy).build();
      httpGet.setConfig(config);
    }
    try (CloseableHttpResponse response = httpclient.execute(httpGet);) {
      Header[] contentTypeHeader = response.getHeaders("Content-Type");
      if (contentTypeHeader != null && contentTypeHeader.length > 0) {
        // 出错
        if (ContentType.TEXT_PLAIN.getMimeType().equals(contentTypeHeader[0].getValue())) {
          String responseContent = Utf8ResponseHandler.INSTANCE.handleResponse(response);
          throw new WxErrorException(responseContent);
        }
      }
      return InputStreamResponseHandler.INSTANCE.handleResponse(response);
    } finally {
      httpGet.releaseConnection();
    }

  }

}
